public class Waiter extends Thread{

    public void getCoffee(){
        synchronized(CoffeeMachine.lock){
            if(CoffeeMachine.coffeeMade==null){

                try {
                    System.out.println("Waiter waiting for coffee to make");
                    Thread.sleep(1000/* 1 second*/);
                    CoffeeMachine.lock.wait();
                } catch (Exception e) {
                    //TODO: handle exception
                    e.printStackTrace();
                }

            }//end if

            System.out.println("Waiter delivering coffee...");
            CoffeeMachine.coffeeMade=null;
            CoffeeMachine.lock.notifyAll();
            System.out.println("Waiter requesting another coffee!!!");
            



        }
    }

    @Override public void run(){
        while(true){

            getCoffee();

        }
    }

}