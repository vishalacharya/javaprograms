

public class App{

    public static void main(String[] arg){
        
        System.out.println("Starting App....");
    
        Waiter waiter=new Waiter();
        CoffeeMachine coffeeMachine=new CoffeeMachine();
        waiter.start();
        coffeeMachine.start();

        try {
            Thread.currentThread().join();    
        } catch (Exception e) {
            //TODO: handle exception
        }

        System.out.println("Terminating App.");
    
    }
        

}

