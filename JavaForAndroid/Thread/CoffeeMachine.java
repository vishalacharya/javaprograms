class CoffeeMachine extends Thread{

    
    public static String coffeeMade=null;
    final  static Object lock= new Object();
    private static int coffeeNumber=1;



    public void makeCoffee(){
        synchronized(CoffeeMachine.lock){
            if(coffeeMade!=null){

                try{
                    System.out.println("Waiter wail till coffee requesting another one");
                    CoffeeMachine.lock.wait();
                    Thread.sleep(1000/*1 second*/);

                }catch(Exception e){
                    e.printStackTrace();
                }

            } //end if

            coffeeMade="Coffee no: "+(coffeeNumber++)+ "just made!!!";
            System.out.println("Coffee machine made coffee no: "+coffeeNumber);
            CoffeeMachine.lock.notifyAll();
            System.out.println("Notifying waiter pickup to coffee no: "+coffeeNumber);
            


        } 
    }

    @Override public void run(){
        while(true){
            try {
                makeCoffee();
                Thread.sleep(3000/* 1 second */);
            } catch (Exception e) {
                //TODO: handle exception
            }
        }
    }


}