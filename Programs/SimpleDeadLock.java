public class SimpleDeadLock{


	/*Resources*/
	String resourceFirst="java";
	String resourceSecond="android";
	/*Creating first thred*/
	Thread threadFirst=new Thread(){
		@Override
		public void run(){
			while(true){
				synchronized(resourceFirst){
					synchronized(resourceSecond){
						System.out.println(resourceFirst + resourceSecond);
					}		
				}		
			}	

		}

	};
	/*Creating second thred*/
	Thread threadSecond=new Thread(){
		@Override
		public void run(){
			while(true){
				synchronized(resourceSecond){
					synchronized(resourceFirst){
						System.out.println(resourceSecond + resourceFirst);
					}		
				}		
			}	

		}

	};


		public static void main(String[] arg){

			SimpleDeadLock simpledeadlock=new SimpleDeadLock();

			simpledeadlock.threadFirst.start();
			simpledeadlock.threadSecond.start();


		}


}