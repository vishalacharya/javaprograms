import java.io.*;

public class SerializationDemo{

	public static void main(String[] arg){


		Item item =new Item();
		try {
            FileOutputStream fos = new FileOutputStream("storage.data");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(item);

            System.out.println("Item is successfully Serialized ");
            System.out.println("Before seriazliation :\t"+item);

            FileInputStream fis = new FileInputStream("storage.data");
            ObjectInputStream ois = new ObjectInputStream(fis);
            Item oldNarnia = (Item) ois.readObject();
         
            System.out.println("Book successfully created from Serialized data");
            System.out.println("Book after seriazliation : " + oldNarnia);
         
        } catch (Exception e) {
            e.printStackTrace();
        }




		
	}
	

}


class Item implements Serializable{

	private String itemName;
	private transient int code=1;//not serialized 
	private int no=1;

	public Item(){
		this("Java for android",122,1);
	}

	public Item(String name,int code,int no){
		this.itemName=name;
		this.code=code;
		this.no=no;
	}

	public String toString(){
		return "\n\t itemName: "+itemName+"\n\t code: "+code+"\n\t no: "+no;
	}


}
