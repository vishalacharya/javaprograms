
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class DuplicateCarHashMap{

	public static void duplicateChar(String str){

		char[] chars=	str.toCharArray();
		HashMap<Character,Integer> charMap=new HashMap<Character,Integer>();
		for(Character chr:chars){
			if(charMap.containsKey(chr)){
				charMap.put(chr,charMap.get(chr)+1);
			}else{
				charMap.put(chr,1);
			}
		}

		Set<Map.Entry<Character,Integer>> sets=charMap.entrySet();
		for(Map.Entry<Character,Integer> entry:sets){
			if(entry.getValue()>1){
				System.out.printf(" %s Duplicate  %s is repeated %n times",entry.getKey(),entry.getValue());
				System.out.println();
			}
		}

	}
	
	public static void main(String[] arg){

		String str1=new String("Programmming");	
		String str2=new String("Combination");	
		String str3=new String("Java");	
		String str4=new String("Yes");	

		// duplicateChar(str1);
		// duplicateChar(str2);
		duplicateChar(str3);
		// duplicateChar(str4);



	}

}
