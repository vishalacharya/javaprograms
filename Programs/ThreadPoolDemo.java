//package com.achobyte.vishal.ThreadPoolDemo;  

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class ThreadPoolDemo{


	public static void main(String[] arg){

		ExecutorService executorservices=Executors.newFixedThreadPool(5);
		for(int i=0;i<5;i++){

			executorservices.execute(new WorkerThread("Thread :"+i));

			}

			executorservices.shutdown();
			try{
				executorservices.awaitTermination(1,TimeUnit.SECONDS);		
			}catch(InterruptedException e){
				e.printStackTrace();
			}


	}


}

class WorkerThread implements Runnable{

	private String name;

	public WorkerThread(String name){

		this.name=name;

	}



	@Override
	public void run(){
		try{
			System.out.println("Thread stated:"+this.name);
			Thread.sleep(10000);	
			System.out.println("Thread completed:"+this.name);

		}catch(InterruptedException  e){
			e.printStackTrace();
		}
	}

}
